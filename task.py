import heapq

def min_cost_to_connect_cables(cables):
    # Перетворити список довжин кабелів в кучу
    heapq.heapify(cables)
    
    total_cost = 0
    
    # Поки в кучі є більше одного елемента
    while len(cables) > 1:
        # Видалити з кучі два найменших елемента і обчислити їх суму
        cable1 = heapq.heappop(cables)
        cable2 = heapq.heappop(cables)
        cost = cable1 + cable2
        
        print(f'Connect {cable1} + {cable2} = {cost}')

        # Додати цю суму до загальних витрат
        total_cost += cost
        
        # Додати обчислену суму назад в кучу
        heapq.heappush(cables, cost)
    
    return total_cost

cables = [8, 4, 6, 12, 2, 10, 14, 16, 1, 2]

print(min_cost_to_connect_cables(cables))